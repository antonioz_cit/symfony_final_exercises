<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Post;

/**
 * PostRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PostRepository extends \Doctrine\ORM\EntityRepository
{
    public function insertPost(Post $post)
    {
        $this->_em->persist($post);
        $this->_em->flush();
    }

    public function updatePost(Post $post)
    {
        $this->_em->flush();
    }

    public function deletePost($id)
    {
        $post = $this->_em->getReference(Post::class, $id);
        
        foreach ($post->getComments() as $comment) {
            $this->_em->remove($comment);
        }

        $this->_em->remove($post);
        $this->_em->flush();
    }
}
