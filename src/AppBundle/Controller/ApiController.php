<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use AppBundle\Entity\Post;
use AppBundle\Form\PostType;
use AppBundle\Form\CommentType;
use AppBundle\Service\PostService;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Api controller
 *
 */
class ApiController extends Controller
{
    private $postService;

    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    /**
     * @ApiDoc(
     *   output = "AppBundle\Entity\Post",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     */
    public function getPostsAction()
    {
        $posts = $this->postService->listPosts();

        return $posts;
    }

    /**
     * @ApiDoc(
     *   output = "AppBundle\Entity\Post",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     */
    public function getPostAction($id)
    {
        $post = $this->postService->getPost($id);

        return $post;
    }

    /**
     * @ApiDoc(
     *   parameters = {
     *      { "name"="post[name]", "dataType"="string", "required"=true }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     */
    public function postPostAction(Request $request)
    {
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->postService->addPost($post);

            return ['message' => 'Data saved successful.'];
        }

        return ['form' => $form];
    }

    /**
     * @ApiDoc(
     *   input = "AppBundle\Form\PostType",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     */
    public function putPostAction(Request $request, $id)
    {
        $post = $this->postService->getPost($id);
        $form = $this->createForm(PostType::class, $post, array('method' => 'PUT'));
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->postService->editPost($post);

            return ['message' => 'Data saved successful.'];
        }

        return ['form' => $form];
    }

    /**
     * @ApiDoc(
     *   input = "int",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     */
    public function deletePostAction($id)
    {
        $this->postService->deletePost($id);

        return ['message' => 'Data deleted successful.'];
    }

    /**
     * @ApiDoc(
     *   parameters = {
     *      { "name"="post_comment[description]", "dataType"="string", "required"=true },
     *      { "name"="post_comment[idPost]", "dataType"="int", "required"=true }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     */
    public function postCommentAction(Request $request)
    {
        $comment = new Comment();
        $commentForm = $this->createForm(CommentType::class, $comment);
        $commentForm->handleRequest($request);
        
        if ($commentForm->isSubmitted() && $commentForm->isValid()) {
            $this->postService->addComment($comment);

            return ['message' => 'Data saved successful.'];
        }

        return ['form' => $commentForm];
    }

    /**
     * @ApiDoc(
     *   input = "int",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     */
    public function deleteCommentAction($id)
    {
        $this->postService->deleteComment($id);
    }
}
