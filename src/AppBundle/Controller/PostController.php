<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Entity\Comment;
use AppBundle\Service\PostService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Post controller.
 *
 * @Route("post")
 */
class PostController extends Controller
{
    private $postService;
    
    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    /**
     * Lists all post entities.
     *
     * @Route("/", name="post_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $posts = $this->postService->listPosts();

        return $this->render('AppBundle:Post:index.html.twig', array(
            'posts' => $posts,
        ));
    }

    /**
     * Creates a new post entity.
     *
     * @Route("/new", name="post_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $post = new Post();
        $form = $this->createForm('AppBundle\Form\PostType', $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->postService->addPost($post);

            return $this->redirectToRoute('post_show', array('id' => $post->getId()));
        }

        return $this->render('AppBundle:Post:register.html.twig', array(
            'post' => $post,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a post entity.
     *
     * @Route("/{id}", name="post_show")
     * @Method("GET")
     */
    public function showAction(Post $post)
    {
        $comment = new Comment();
        $comment->setIdPost($post->getId());
        $commentForm = $this->createCommentForm($comment);

        return $this->render('AppBundle:Post:show.html.twig', array(
            'post' => $post,
            'comment_form' => $commentForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing post entity.
     *
     * @Route("/{id}/edit", name="post_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Post $post)
    {
        $editForm = $this->createForm('AppBundle\Form\PostType', $post);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->postService->editPost($post);

            return $this->redirectToRoute('post_show', array('id' => $post->getId()));
        }

        return $this->render('AppBundle:Post:register.html.twig', array(
            'post' => $post,
            'form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a post entity.
     *
     * @Route("/{id}", name="post_delete")
     * @Method("POST")
     */
    public function deleteAction(Request $request, $id)
    {
        $this->postService->deletePost($id);

        return new Response();
    }

    /**
     * Add comment a post entity.
     *
     * @Route("/comment/add", name="post_comment_add")
     * @Method("POST")
     */
    public function commentAction(Request $request)
    {
        $comment = new Comment();
        $commentForm = $this->createCommentForm($comment);
        $commentForm->handleRequest($request);

        if ($commentForm->isSubmitted() && $commentForm->isValid()) {
            $this->postService->addComment($comment);

            return $this->redirectToRoute('post_show', array('id' => $comment->getIdPost()));
        }

        return $this->render('AppBundle:Post:show.html.twig', array(
            'post' => $post,
            'comment_form' => $commentForm->createView(),
        ));
    }
    
    /**
     * Deletes a comment entity.
     *
     * @Route("comment/{idComment}/delete", name="post_comment_delete")
     * @Method("POST")
     */
    public function deleteCommentAction(Request $request, $idComment)
    {
        $this->postService->deleteComment($idComment);
     
        return new Response();
    }

    /**
     * Creates a form to add comment a post entity.
     *
     * @param Post $post The post entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCommentForm(Comment $comment)
    {
        return $this->createForm('AppBundle\Form\CommentType', $comment, array(
            'action' => $this->generateUrl('post_comment_add'),
            'method' => 'POST'
        ));
    }
}
