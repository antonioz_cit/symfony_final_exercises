<?php

namespace AppBundle\Service;

use AppBundle\Entity\Post;
use AppBundle\Entity\Comment;
use AppBundle\Repository\PostRepository;
use AppBundle\Repository\CommentRepository;

class PostService
{
    private $postRepository;
    private $commentRepository;

    public function __construct(PostRepository $postRepository, CommentRepository $commentRepository)
    {
        $this->postRepository = $postRepository;
        $this->commentRepository = $commentRepository;
    }

    public function listPosts()
    {
        $posts = $this->postRepository->findAll();

        return $posts;
    }

    public function getPost($id)
    {
        $post = $this->postRepository->find($id);
        
        return $post;
    }

    public function addPost(Post $post)
    {
        $post->setCreateAt(new \DateTime('now'));

        $this->postRepository->insertPost($post);
    }

    public function deletePost($id)
    {
        $this->postRepository->deletePost($id);
    }

    public function editPost(Post $post)
    {
        $this->postRepository->updatePost($post);
    }

    public function addComment(Comment $comment)
    {
        $comment->setCreateAt(new \DateTime('now'));

        $this->commentRepository->insertComment($comment);
    }

    public function deleteComment($id)
    {
        $this->commentRepository->deleteComment($id);
    }

}
